package com.fatunicorn.euler0001;

import java.util.ArrayList;
import java.util.List;

/**
 * finds the sum of all the multiples of args[1...n] below args[0]
 */
public class Euler0001 {

    public static void main(String[] args) {

        if(args.length < 2) {
            System.out.println("Enter at least two parameters.");
            System.exit(0);
        }

        if(new Integer(args[0]) < 0) {
            System.out.println("Multiplier limit must be greater than 0.");
            System.exit(0);
        }

	    Solver theSolver = new Solver(new Integer(args[0]));
        for(int i=1; i<args.length; i++) {
            theSolver.addMultiplier(new Integer(args[i]));
        }
	    System.out.println( "The result is: " + theSolver.compute() );
    }

    /**
     * solver for the problem
     */
    private static class Solver {
        List<Integer> multipliers = new ArrayList<Integer>();
        Integer multipleLimit;

        Solver(Integer multipleLimit) {
            this.multipleLimit = multipleLimit;
        }

        /**
         * computs the sum of all multiples of the provided multipliers
         * @return Integer
         */
        Integer compute() {
            Integer result = 0;
            for(int testnumber=1; testnumber <= multipleLimit; testnumber++) {
                for(Integer multiplier: multipliers) {
                    if((testnumber % multiplier) == 0) {
                        result += testnumber;
                        break;
                    }
                }
            }
            return result;
        }

        /**
         * adds an additional multiplier
         *
         * @param inMultiplier Integer
         */
        void addMultiplier(Integer inMultiplier) {
            multipliers.add(inMultiplier);
        }
    }
}
